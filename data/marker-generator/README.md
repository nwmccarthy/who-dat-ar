Program downloaded from http://flash.tarotaro.org/blog/2009/07/12/mgo2/

Use on firefox, as chrome seems to be flacky


--------------------------------------------

How to use


- Design your original markers and print it.
- open ARToolKit Marker Generator Online Multi.
- Set segments and marker size.
- Point your webcam at the printed markers.
- Push “Get Pattern” button when a red line encloses the markers. and go “save mode”.
- When “save mode” starts, Preview window appears.
- Red squares show all of detected markers.
- Green square shows the marker in the preview now.
- Preview Window has 6 buttons.
    - Push “Prev/Next” button to change marker.
    - Push “Delete” button to exclude current previewing marker from target.
    - Push “Save All” button to save all pattern file “*.zip”.
    - Push “Save Current” button to save previewing pattern file “*.pat”.
    - Push “Cancel” button or close window to end “save mode”.
